import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import {AppMaterialModule} from './app-material.module';

// Components
import {AppComponent} from './app.component';

// Pessoa Component
import {PessoaAddComponent} from './components/pessoa-add/pessoa-add.component';
import {PessoaEditComponent} from './components/pessoa-edit/pessoa-edit.component';
import {PessoaFormComponent} from './components/pessoa-form/pessoa-form.component';
import {PessoaListComponent} from './components/pessoa-list/pessoa-list.component';



// Services
import {ApiService} from './services/api.service';
import {PessoaService} from './services/pessoa.service';


@NgModule({
  declarations: [
    AppComponent,
    PessoaListComponent,
    PessoaFormComponent,
    PessoaEditComponent,
    PessoaAddComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NoopAnimationsModule,
    AppMaterialModule
  ],
  providers: [
    ApiService,
    PessoaService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
