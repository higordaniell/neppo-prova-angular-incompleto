import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import {PessoaListComponent} from './components/pessoa-list/pessoa-list.component';
import {PessoaEditComponent} from './components/pessoa-edit/pessoa-edit.component';
import {PessoaAddComponent} from './components/pessoa-add/pessoa-add.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: PessoaListComponent },
  { path: 'pessoas', component: PessoaListComponent },
  { path: 'pessoa/add', component: PessoaAddComponent },
  { path: 'pessoa/:id', component: PessoaEditComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
