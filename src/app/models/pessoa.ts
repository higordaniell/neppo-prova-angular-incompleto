export class Pessoa {
  id?: number;
  nome?: string;
  dataNascimento?: Date;
  identificacao?: number;
  sexo?: number;
  endereco?: string;
  created_at?: Date;
  updated_at?: Date;
  url?: string;
}
