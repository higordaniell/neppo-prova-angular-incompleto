import {Component, OnInit} from '@angular/core';

// Models
import {Pessoa} from '../../models/pessoa';

// Services
import {PessoaService} from '../../services/pessoa.service';

@Component({
  selector: 'app-pessoa-list',
  templateUrl: './pessoa-list.component.html',
  styleUrls: ['./pessoa-list.component.scss']
})
export class PessoaListComponent implements OnInit {

  pessoas: Pessoa[];
  loading: boolean;
  error: any;

  constructor(public pessoaService: PessoaService) {
  }

  ngOnInit() {
    this.loading = true;
    this.pessoaService.getPessoas().subscribe(
      (pessoas) => {
        this.pessoas = pessoas;
      },
      (err: any) => {
        alert('Falha ao carregar pessoas!');
        this.error = err;
        console.log('Error', err);
      },
      () => {
        this.loading = false;
      }
    );
  }

  apagar(pessoa: Pessoa) {
    let confirm = window.confirm('Você tem certeza que deseja apagar a pessoa \'' + pessoa.nome + '\'?');
  }

}
