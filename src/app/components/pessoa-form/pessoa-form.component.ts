import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

import {FormGroup, FormControl, Validators} from '@angular/forms';

// Models
import {Pessoa} from '../../models/pessoa';

@Component({
  selector: 'app-pessoa-form',
  templateUrl: './pessoa-form.component.html',
  styleUrls: ['./pessoa-form.component.scss']
})
export class PessoaFormComponent implements OnInit {

  @Input() pessoa: Pessoa;
  @Input() nova: boolean;
  @Output() pessoaSalva = new EventEmitter<Pessoa>();

  rForm: FormGroup;

  constructor() {
  }

  /*
  id?: number;
  nome?: string;
  dataNascimento?: Date;
  identificacao?: number;
  sexo?: number;
  endereco?: string;
  created_at?: Date;
  updated_at?: Date;
  url?: string;
  */

  ngOnInit() {
    this.rForm = new FormGroup({
      'nome': new FormControl(this.pessoa.nome, [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(100),
      ]),
      'dataNascimento': new FormControl(this.pessoa.dataNascimento, Validators.required),
      'identificacao': new FormControl(this.pessoa.identificacao, Validators.required),
      'sexo': new FormControl(this.pessoa.sexo, [
        Validators.required,
        Validators.min(0),
        Validators.max(2)
      ]),
      'endereco': new FormControl(this.pessoa.endereco, [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(200),
      ])
    });
  }

  salvar() {

    console.log(this.rForm);

    if (!this.rForm.valid) {
      return;
    }

    this.pessoa = this.rForm.value;
    this.pessoaSalva.emit(this.pessoa);
  }
}
