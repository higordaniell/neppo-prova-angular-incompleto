import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

// Model
import {Pessoa} from '../../models/pessoa';

// Service
import {PessoaService} from '../../services/pessoa.service';

@Component({
  selector: 'app-pessoa-edit',
  templateUrl: './pessoa-edit.component.html',
  styleUrls: ['./pessoa-edit.component.scss']
})
export class PessoaEditComponent implements OnInit {

  pessoa: Pessoa;

  constructor(private pessoaService: PessoaService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.loadPessoa(id);
  }

  loadPessoa(id) {
    this.pessoaService.getPessoa(id).subscribe((p) => {
      this.pessoa = p;
    });
  }

  editar() {
    console.log('Agora enviar pro servidor');
  }

}
