import { Component, OnInit } from '@angular/core';

// Model
import {Pessoa} from '../../models/pessoa';

@Component({
  selector: 'app-pessoa-add',
  templateUrl: './pessoa-add.component.html',
  styleUrls: ['./pessoa-add.component.scss']
})
export class PessoaAddComponent implements OnInit {

  pessoa: Pessoa = {
    nome: 'Nome'
  };

  constructor() { }

  ngOnInit() {
  }

  add($event) {
    console.log( 'Enviar pro servidor');
  }
}
