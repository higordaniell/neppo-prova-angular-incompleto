import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, tap } from 'rxjs/operators';

// Api
import {ApiService} from './api.service';

// Models
import {Pessoa} from '../models/pessoa';

@Injectable()
export class PessoaService {

  constructor(private api: ApiService) { }

  /** GET people from the server */
  getPessoas (): Observable<Pessoa[]> {
    return this.api.get<Pessoa[]>( 'pessoas.json' ).pipe(
        tap(pessoas => this.log(`fetched pessoas`)),
        catchError(this.handleError('getPessoas', []))
      );
  }

  /** GET person by id */
  getPessoa(id): Observable<Pessoa> {
    const url = `pessoas/${id}.json`;
    return this.api.get<Pessoa>(url).pipe(
      tap(_ => this.log(`fetched person id=${id}`)),
      catchError(this.handleError<Pessoa>(`getPessoa id=${id}`))
    );
  }

  /** POST: add a new person to the server */
  addPessoa(pessoa: Pessoa): Observable<Pessoa> {
    return this.api.post<Pessoa>('pessoas', pessoa).pipe(
      tap((p: Pessoa) => this.log(`added person w/ id=${p.id}`)),
      catchError(this.handleError<Pessoa>('addPessoa'))
    );
  }

  /** DELETE: delete the person from the server */
  deletePessoa (pessoa: Pessoa | number): Observable<Pessoa> {
    const id = typeof pessoa === 'number' ? pessoa : pessoa.id;
    const url = `pessoas/${id}`;

    return this.api.delete<Pessoa>(url).pipe(
      tap(_ => this.log(`deleted person id=${id}`)),
      catchError(this.handleError<Pessoa>('deletePessoa'))
    );
  }

  /** PUT: update the person on the server */
  updatePessoa (pessoa: Pessoa): Observable<any> {
    return this.api.put('pessoas', pessoa).pipe(
      tap(_ => this.log(`updated person id=${pessoa.id}`)),
      catchError(this.handleError<any>('updatePessoa'))
    );
  }




  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    console.log(message);
  }
}
