import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestMethod } from '@angular/http';

// Http Options
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class ApiService {

  API_URL = 'https://test-frontend-neppo.herokuapp.com/';

  constructor(private http: HttpClient) { }

  private url(url: string) {
    return this.API_URL + url;
  }

  public get<T>(url: string) {
    return this.http.get<T>(this.url(url), httpOptions);
  }

  public post<T>(url: string, body: any) {
    return this.http.post<T>(this.url(url), body, httpOptions);
  }

  public put<T>(url: string, body: any) {
    return this.http.put<T>(this.url(url), body, httpOptions);
  }

  public delete<T>(url: string) {
    return this.http.delete<T>(this.url(url), httpOptions);
  }

  public patch<T>(url: string, body: any) {
    return this.http.patch<T>(this.url(url), body, httpOptions);
  }

  public head(url: string) {
    return this.http.head(this.url(url), httpOptions);
  }

  public options(url: string) {
    return this.http.options(this.url(url), httpOptions);
  }

}
